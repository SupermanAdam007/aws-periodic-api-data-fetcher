provider "aws" {
  region  = "eu-central-1"
}

## Lambda function
resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

data "archive_file" "fetch_data_lambda_zip" {
  type        = "zip"
  source_dir  = "../code/fetch_data_lambda"
  output_path = "fetch_data_lambda.zip"
}

resource "aws_lambda_function" "fetch_data_lambda" {
  filename         = "fetch_data_lambda.zip"
  function_name    = "fetch_data_lambda"
  source_code_hash = data.archive_file.fetch_data_lambda_zip.output_base64sha256
  role             = aws_iam_role.iam_for_lambda.arn
  handler          = "download_data_into_s3.handler"
  runtime          = "python3.8"
  depends_on       = [aws_iam_role_policy_attachment.lambda_logs, aws_cloudwatch_log_group.fetch_data_lambda_logs]
  timeout          = 60
}

## Lambda trigger
resource "aws_cloudwatch_event_rule" "periodic_trigger" {
  name                = "periodic_trigger"
  description         = "Fires every five minutes"
  schedule_expression = "rate(5 minutes)"
}

resource "aws_cloudwatch_event_target" "fetch_data_lambda_periodic_trigger" {
  rule      = aws_cloudwatch_event_rule.periodic_trigger.name
  target_id = "fetch_data_lambda"
  arn       = aws_lambda_function.fetch_data_lambda.arn
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_fetch_data_lambda" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.fetch_data_lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.periodic_trigger.arn
}

## Lambda logging
resource "aws_cloudwatch_log_group" "fetch_data_lambda_logs" {
  name              = "/aws/lambda/fetch_data_lambda"
  retention_in_days = 7
}

resource "aws_iam_policy" "lambda_logging" {
  name        = "lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}

## For S3 access
resource "aws_iam_role_policy_attachment" "s3_iam_for_lambda" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}