from datetime import date, timedelta
import json
import urllib3

import boto3


DATASET_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports"
DATETIME_FORMAT = "%m-%d-%Y"
MAX_DAYS_BEFORE_YESTERDAY = 20
S3_BUCKET_NAME = "fetch-data-lambda-output"


def get_list_of_dates_to_downloads():
    dates_to_downloads = []
    for minus_days in range(1, MAX_DAYS_BEFORE_YESTERDAY + 1):
        datetime = date.today() - timedelta(days=minus_days)
        datetime_formatted = datetime.strftime(DATETIME_FORMAT)
        dates_to_downloads.append(datetime_formatted)
    return dates_to_downloads


def save_dataset_to_s3(data, datetime_formatted):
    s3 = boto3.resource("s3")
    bucket = s3.Bucket(S3_BUCKET_NAME)

    data_dir = "/tmp"
    f_name = f"covid-data-{datetime_formatted}"

    with open(f"{data_dir}/{f_name}", "wb") as f:
        f.write(data)
        bucket.upload_file(f"{data_dir}/{f_name}", f_name)


def handler(event, context):
    http = urllib3.PoolManager()

    list_of_dates_to_downloads = get_list_of_dates_to_downloads()

    requests_status_codes = dict()

    for date_to_download in list_of_dates_to_downloads:
        download_url = f"{DATASET_URL}/{date_to_download}.csv"
        res = http.request("GET", download_url)

        print(f"status code: {res.status}, download URL: {download_url}")

        if res.status != 200:
            print(f'date: "{date_to_download}" failed')

        if res.status in requests_status_codes:
            requests_status_codes[res.status].append(download_url)
        else:
            requests_status_codes[res.status] = [download_url]

        save_dataset_to_s3(
            data=res.data.decode("utf-8"), datetime_formatted=date_to_download
        )

    print(json.dumps(requests_status_codes, indent=2, ensure_ascii=False))

    return requests_status_codes


# for testing purpose only
if __name__ == "__main__":
    handler(None, None)
