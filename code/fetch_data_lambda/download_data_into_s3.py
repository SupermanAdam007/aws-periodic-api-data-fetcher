import csv
from datetime import date, timedelta
import os
import time
import urllib3

import boto3
import botocore


DATASET_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports"
DATETIME_FORMAT = "%m-%d-%Y"
S3_BUCKET_NAME = "fetch-data-lambda-output"

ATHENA_DB_NAME = "db_covid_data"
ATHENA_TABLE_NAME = "table_covid_data"
ATHENA_S3_RESULT_PATH = "s3://fetch-data-lambda-output/report"

client_athena = boto3.client("athena")
s3 = boto3.resource("s3")
bucket = s3.Bucket(S3_BUCKET_NAME)

HTML_REPORT_TEMPLATE = """
<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>
TABLE
</body>
</html>
"""


def get_latest_date_to_download():
    print("get_latest_date_to_download")

    datetime_yesterday = date.today() - timedelta(days=1)
    datetime_formatted = datetime_yesterday.strftime(DATETIME_FORMAT)
    return datetime_formatted


def save_data_to_s3(data, datetime_formatted):
    print("save_data_to_s3")

    s3 = boto3.resource("s3")

    data_dir = "/tmp"
    f_name = f"covid-data-{datetime_formatted}.csv"

    with open(f"{data_dir}/{f_name}", "w") as f:
        f.write(data)
        bucket.upload_file(f"{data_dir}/{f_name}", f"covid-dataset/{f_name}")


def wait_for_athena_response(athena_query_start, max_execution=20):
    print("wait_for_athena_response")

    execution_id = athena_query_start["QueryExecutionId"]
    state = "RUNNING"

    while max_execution > 0 and state in ["RUNNING", "QUEUED"]:
        max_execution = max_execution - 1
        response = client_athena.get_query_execution(QueryExecutionId=execution_id)

        if (
            "QueryExecution" in response
            and "Status" in response["QueryExecution"]
            and "State" in response["QueryExecution"]["Status"]
        ):
            state = response["QueryExecution"]["Status"]["State"]
            print(f"wait_for_athena_response: {state}")

            if state == "FAILED":
                return ""
            elif state == "SUCCEEDED":
                s3_key = execution_id + ".csv"
                local_filename = "/tmp/index.csv"
                try:
                    bucket.download_file(f"report/{s3_key}", local_filename)
                except botocore.exceptions.ClientError as e:
                    if e.response["Error"]["Code"] == "404":
                        print("The object does not exist.")
                    else:
                        raise
                return local_filename
        time.sleep(0.2)


def get_table_from_csv(csv_filename):
    print("get_table_from_csv")

    table = "<table>"
    with open(csv_filename, encoding="utf8") as csv_file:
        reader = csv.DictReader(csv_file, delimiter=",")
        table += f"<tr>{''.join([f'<td>{header}</td>' for header in reader.fieldnames])}</tr>"
        for row in reader:
            table_row = "<tr>"
            for fn in reader.fieldnames:
                table_row += f"<td>{row[fn]}</td>"
            table_row += "</tr>"
            table += table_row
    table += "</table>"

    if os.path.isfile(csv_filename):
        os.remove(csv_filename)

    return HTML_REPORT_TEMPLATE.replace("TABLE", table)


def athena_create_table():
    print("athena_create_table")

    athena_query_start = client_athena.start_query_execution(
        QueryString="""
CREATE EXTERNAL TABLE IF NOT EXISTS db_covid_data.table_covid_data (
        `FIPS` STRING,
        `Admin2` STRING,
        `Province_State` STRING,
        `Country_Region` STRING,
        `Last_Update` STRING,
        `Lat` STRING,
        `Long_` STRING,
        `Confirmed` STRING,
        `Deaths` STRING,
        `Recovered` STRING,
        `Active` STRING,
        `Combined_Key` STRING
    )
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
    'serialization.format' = '1'
)
LOCATION 's3://fetch-data-lambda-output/covid-dataset'
TBLPROPERTIES (
    'skip.header.line.count' = '1'
)
""",
        QueryExecutionContext={"Database": "db_covid_data"},
        ResultConfiguration={"OutputLocation": ATHENA_S3_RESULT_PATH},
    )
    time.sleep(2)


def query_country_region(country_region="Czechia"):
    print("query_country_region")

    athena_query_start = client_athena.start_query_execution(
        QueryString="""
SELECT * FROM db_covid_data.table_covid_data
WHERE country_region='Czechia'
LIMIT 100;
""",
        QueryExecutionContext={"Database": "db_covid_data"},
        ResultConfiguration={"OutputLocation": ATHENA_S3_RESULT_PATH},
    )

    return wait_for_athena_response(athena_query_start)


def save_report_to_s3(local_f_name, bucket_f_name):
    print("save_report_to_s3")

    boto3.client("s3").upload_file(
        local_f_name,
        S3_BUCKET_NAME,
        bucket_f_name,
        ExtraArgs={"ContentType": "text/html"},
    )


def update_report():
    print("update_report")

    athena_create_table()
    csv_filename = query_country_region()

    if csv_filename:
        table = get_table_from_csv(csv_filename)
    else:
        return

    if table:
        with open("/tmp/index.html", "w") as f:
            f.write(table)

        # with open("/tmp/index.html", "r") as f:
        #     data = f.read()
        #     print(f"/tmp/index.html file: {data}")
        #     print(data)

    save_report_to_s3(local_f_name="/tmp/index.html", bucket_f_name="index.html")


def handler(event, context):
    print("handler")

    http = urllib3.PoolManager()

    date_to_download = get_latest_date_to_download()

    download_url = f"{DATASET_URL}/{date_to_download}.csv"
    res = http.request("GET", download_url)

    print(f"status code: {res.status}, download URL: {download_url}")

    if res.status != 200:
        print(f'date: "{date_to_download}" failed')
        return {"success": False}

    save_data_to_s3(data=res.data.decode("utf-8"), datetime_formatted=date_to_download)

    update_report()

    return {"success": True}


# for testing purpose only
if __name__ == "__main__":
    handler(None, None)
