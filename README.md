# AWS serverless periodic API data fetcher

## This repository contains
- AWS resource definitions in Terraform (S3, Lambda, Athena)
- Code for lambda function
- Simple CI/CD GitLab pipeline

## Architecture
- CloudWatch periodically triggers lambda function (cloudwatch_event_rule)
- Lambda function does 3 things:
    1. Downloads dataset from https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data/csse_covid_19_daily_reports to S3
    2. Query the dataset with Athena
    3. Save the generated report into simple html table to S3: 
- Lambda function is monitored via CloudWatch (metrics, logs)
- All cloud resources are automated with Terraform ecosystem

## How to run it
With system logged in to the AWS (env. variables `AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY`) run: 
```bash>
cd infrastructure
terraform init
terraform apply
```
- example report for Athena query data (`country_region='Czechia'`) can be shown on link: https://fetch-data-lambda-output.s3.eu-central-1.amazonaws.com/index.html (simple html table)

## Possible improvements
- Do not hardcode queries and report template into the lambda code
- Make public only resources which should be (S3 endpoint to the report)
- S3 backup and deletion of tmp data 
- Specifify closely CloudWatch timing for lambda function trigger. The used dataset is updating also periodically.
- more lambda code comments + code clean
